/**
 * Created by jacek on 28.12.16.
 */

const middleware = require('./middleware');
const schema = require('./schemas');

module.exports = {
    validatorFactory: middleware.factory,
    defaultErrorHandler: middleware.errorHandler,
    schema : schema
};