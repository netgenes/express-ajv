/**
 * Created by jacek on 29.12.16.
 */

const Ajv = require('ajv');
const ajv = new Ajv();

const schemas = {};

const registerSchema = ( schemaId, schema ) => {

    if ( typeof schemas[schemaId] === 'object') {

        throw new Error(`JSON Schema '${schemaId}' is already registered.`)
    }

    schemas[schemaId] = {
        raw : schema
    };

    ajv.addSchema(schema,schemaId);
};

const getSchemaValidator = schemaId => {

    let schema = schemas[schemaId];

    if ( !schema ) {

        throw new Error(`JSON Schema '${schemaId}' isn't registered.`);
    }

    if ( !schema.validator ) {

        schema.validator = ajv.getSchema( schemaId );
    }

    return schema.validator;
};

const getRawSchema = schemaId => {

    let schema = schemas[schemaId];

    if ( !schema ) {

        throw new Error(`JSON Schema '${schemaId}' isn't registered.`);
    }

    return schema.raw;
};

const addCustomKeyword = ( keyword, definition ) => {

    ajv.addKeyword( keyword, definition );
};

const addCustomFormat = ( formatName, formatChecker ) => {

    ajv.addFormat( formatName, formatChecker );
};

exports.addSchema = registerSchema;
exports.getSchema = getRawSchema;
exports.getValidator = getSchemaValidator;

exports.addKeyword = addCustomKeyword;
exports.addFormat = addCustomFormat;

exports.__schemas = schemas;