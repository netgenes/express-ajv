/**
 * Created by jacek on 29.12.16.
 */

const describe = require('mocha').describe;
const it = require('mocha').it;
const expect = require('expect.js');

describe('Schema interface', () => {

    const schema = require('../index').schema;

    const testSchema = {
        properties: {
            a: {
                type: 'number'
            }
        }
    };

    it('schema should be accessible through module schema property', () => {

        expect(schema).to.be.ok();
    });

    it('schema should has methods addSchema, addKeyword, getSchema, getValidator', () => {

        expect(schema).to.have.property('addSchema');
        expect(schema.addSchema).to.be.a('function');
        expect(schema).to.have.property('addKeyword');
        expect(schema.addKeyword).to.be.a('function');
        expect(schema).to.have.property('getValidator');
        expect(schema.getValidator).to.be.a('function');
        expect(schema).to.have.property('getSchema');
        expect(schema.getSchema).to.be.a('function');
    });

    it('schema should has __schemas property', () => {

        expect(schema).to.have.property('__schemas');
        expect(schema.__schemas).to.be.an('object');
    });

    it('schema validator should be created after getValidator call', () => {

        schema.addSchema('test', testSchema);
        expect(schema.__schemas.test).to.be.ok();
        expect(schema.__schemas.test.validator).to.not.be.ok();

        schema.getValidator('test');

        expect(schema.__schemas.test.validator).to.be.ok();
    });

    it('try to get unregistered schema or validator should throw exception', () => {

        expect(schema.getSchema).withArgs('undefined').to.throwException(e => expect(e).to.be.an(Error));
    });

    it('try to override registered schema should throw exception', ( ) => {
        //first call
        expect(schema.addSchema).withArgs('test1',testSchema).to.not.throwException();
        //second call
        expect(schema.addSchema).withArgs('test1',testSchema).to.throwException(/already registered/);
    });
});