/**
 * Created by jacek on 04.01.17.
 */
const httpMocks = require('node-mocks-http');
describe('Referenced Schema', () => {

    it('Must works', done => {

        const expressAjv = require('../index');

        expressAjv.schema.addSchema('i18n',require('./mock/i18n.json'));
        expressAjv.schema.addSchema('test4',require('./mock/ref.json'));

        const validatorFactory = require('../lib/validator');

        var request = httpMocks.createRequest({
            method: 'POST',
            body: {
                "name": [{
                    "lang":"pl",
                    "text":"dom"
                },{
                    "lang":"en",
                    "text":"home"
                }]
            }
        });

        var response = httpMocks.createResponse();

        validatorFactory('test4')(request,response).then(t=>t && done()).catch(done);
    });
});