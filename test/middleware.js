/**
 * Created by jacek on 29.12.16.
 */
const describe = require('mocha').describe;
const it = require('mocha').it;
const expect = require('expect.js');
const httpMocks = require('node-mocks-http');
const testSchema = require('./mock/test-schema');


describe('middleware', () => {

    const expressAjv = require('../index');
    expressAjv.schema.addSchema('verify', testSchema);


    it('this should behave like middleware', done => {

        const middlewareFunction = expressAjv.validatorFactory('verify');

        expect(middlewareFunction).to.be.a('function');

        const validRequest = httpMocks.createRequest({method:'POST',body:{a:1}});
        const response = httpMocks.createResponse();

        middlewareFunction(validRequest, response, done );
    });

    it('this should delegate errors through next', done => {

        const middlewareFunction = expressAjv.validatorFactory('verify');

        expect(middlewareFunction).to.be.a('function');

        const invalidRequest = httpMocks.createRequest({method:'POST',body:{a:'Some invalid value'}});
        const response = httpMocks.createResponse();

        middlewareFunction(invalidRequest, response, (e) => {

            expect(e).to.be.ok();
            done()
        });
    });
});