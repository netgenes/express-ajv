/**
 * Created by jacek on 28.12.16.
 */

var httpMocks = require('node-mocks-http');
var expect = require('expect.js');
var describe = require('mocha').describe;
var it = require('mocha').it;

describe('Package', () => {

    const expressAjv = require('../index');

    it('package should have schema, factory, and defaultErrorHandler', () => {

        expect(expressAjv).to.have.property('validatorFactory');
        expect(expressAjv).to.have.property('defaultErrorHandler');
        expect(expressAjv).to.have.property('schema');
    });
});