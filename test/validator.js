/**
 * Created by jacek on 29.12.16.
 */

const describe = require('mocha').describe;
const it = require('mocha').it;
const expect = require('expect.js');
const httpMocks = require('node-mocks-http');

describe('Validator factory', function() {

    const schema = require('../index').schema;
    const validatorFactory = require('../lib/validator');

    it('should skip validation on safe methods', function (done) {

        done();
    });

    it('schema', function(done) {

        var request = httpMocks.createRequest({
            method: 'POST',
            body: {
                user_id: 1,
                user_name: 'john doe'
            }
        });

        var response = httpMocks.createResponse();


        schema.addSchema('test3',{
            'additionalProperties' : false,
            'properties': {
                user_id: {
                    type: 'number'
                },
                user_name: {
                    type: 'string'
                }
            }
        });

        validatorFactory('test3')(request, response).then(t=>t && done()).catch(done);
    });

    it('custom', function( done ) {

        var request = httpMocks.createRequest({

            method: 'POST',
            body: {
                ping: 'pong'
            }
        });

        var response = httpMocks.createResponse();

        schema.addKeyword('ping', { type: 'string', compile: () => d => d === 'pong'});

        schema.addSchema('test2', {
            'additionalProperties' : false,
            'properties': {
                'ping': {
                    ping: true
                }
            }
        });

        validatorFactory('test2')(request, response).then(t=>t && done()).catch(done);
    });
});
